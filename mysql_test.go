package squalus

import (
	"context"
	"database/sql"
	"testing"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/assert"
)

func ConnectToMysql(t *testing.T) (string, DB, *sql.DB) {
	db, err := sql.Open("mysql", "u:p@tcp(127.0.0.1:13306)/db?parseTime=true")
	errPing := db.Ping()
	if err != nil || errPing != nil {
		db, err = sql.Open("mysql", "u:p@tcp(mysql:3306)/db?parseTime=true")
		if err != nil {
			t.Fatalf("Failed to connect to Mysql server: %s", err)
		}
	}
	err = db.Ping()
	if err != nil {
		t.Fatalf("Failed to connect to Mysql server: %s", err)
	}
	sdb, err := NewDB(db)
	if err != nil {
		t.Fatalf("Failed to create Squalus DB: %s", err)
	}

	_, err = sdb.Exec(context.Background(), "drop table if exists [persons]", nil)
	assert.NoError(t, err, "Table destruction should succeed")
	_, err = sdb.Exec(
		context.Background(),
		"create table [persons]([id] int PRIMARY KEY, [name] varchar(128), [height] float, [birth] datetime)",
		nil,
	)
	assert.NoError(t, err, "Table creation should succeed")

	_, err = sdb.Exec(context.Background(), "drop table if exists [testbinary]", nil)
	assert.NoError(t, err, "Table destruction should succeed")
	_, err = sdb.Exec(context.Background(), "create table [testbinary]([data] blob)", nil)
	assert.NoError(t, err, "Table creation should succeed")

	return "mysql", sdb, db
}

func TestMysqlContext(t *testing.T) {
	_, sdb, _ := ConnectToMysql(t)
	defer func() { assert.NoError(t, sdb.Close()) }()

	ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond)
	defer cancel()
	_, err := sdb.Exec(
		ctx,
		"SELECT SLEEP(1)", // Argument to SLEEP is in seconds
		nil,
	)
	assert.Error(t, err)
	assert.Equal(t, context.DeadlineExceeded, ctx.Err())
}

func TestMysqlUnsuportedIsolationLevel(t *testing.T) {
	_, sdb, _ := ConnectToMysql(t)
	defer func() { assert.NoError(t, sdb.Close()) }()

	_, err := sdb.Begin(context.Background(), &sql.TxOptions{Isolation: sql.LevelSnapshot})
	assert.Error(t, err)
}

func TestMysqlUpsert(t *testing.T) {
	_, sdb, _ := ConnectToMysql(t)
	defer func() { assert.NoError(t, sdb.Close()) }()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	_, err := sdb.Exec(
		ctx,
		"INSERT INTO [persons] ([id], [name]) VALUES ({id}, {name}) ON DUPLICATE KEY UPDATE [name] = {name}",
		map[string]interface{}{
			"id":   100,
			"name": "NAME",
		},
	)
	assert.NoError(t, err)
}
