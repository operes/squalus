package squalus

import (
	"context"
	"database/sql"
	"os"
	"testing"
	"time"

	_ "github.com/mattn/go-sqlite3"
	"github.com/stretchr/testify/assert"
)

func ConnectToSQLite(t *testing.T) (string, DB, *sql.DB) {
	_ = os.Remove("./test.db") // ignore errors (an error is returned if the file did not exist)
	db, err := sql.Open("sqlite3", "./test.db")
	if err != nil {
		t.Fatalf("Failed to connect to SQLite: %s", err)
	}
	err = db.Ping()
	if err != nil {
		t.Fatalf("Failed to connect to SQLite server: %s", err)
	}
	sdb, err := NewDB(db)
	if err != nil {
		t.Fatalf("Failed to create Squalus DB: %s", err)
	}
	_, err = sdb.Exec(context.Background(), "drop table if exists [persons]", nil)
	assert.NoError(t, err, "Table destruction should succeed")
	_, err = sdb.Exec(
		context.Background(),
		"create table [persons]([id] int PRIMARY KEY, [name] varchar(128), [height] float, [birth] datetime)",
		nil,
	)
	assert.NoError(t, err, "Table creation should succeed")

	_, err = sdb.Exec(context.Background(), "drop table if exists testbinary", nil)
	assert.NoError(t, err, "Table destruction should succeed")
	_, err = sdb.Exec(context.Background(), "create table testbinary([data] blob)", nil)
	assert.NoError(t, err, "Table creation should succeed")

	return "sqlite", sdb, db
}

func TestSQLiteUpsert(t *testing.T) {
	_, sdb, _ := ConnectToSQLite(t)
	defer func() { assert.NoError(t, sdb.Close()) }()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	_, err := sdb.Exec(
		ctx,
		"INSERT INTO [persons] ([id], [name]) VALUES ({id}, {name}) "+
			"ON CONFLICT([id]) DO UPDATE SET [name] = {name}",
		map[string]interface{}{
			"id":   100,
			"name": "NAME",
		},
	)
	assert.NoError(t, err)
}
