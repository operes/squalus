package squalus

import (
	"context"
	"database/sql"
	"testing"
	"time"

	_ "github.com/lib/pq"
	"github.com/stretchr/testify/assert"
)

func ConnectToPostgresql(t *testing.T) (string, DB, *sql.DB) {
	db, err := sql.Open("postgres", "user=u password=p dbname=db port=15432 sslmode=disable")
	errPing := db.Ping()
	if err != nil || errPing != nil {
		db, err = sql.Open("postgres", "user=u password=p dbname=db host=postgres port=5432 sslmode=disable")
		if err != nil {
			t.Fatalf("Failed to connect to Postgresql server: %s", err)
		}
	}
	err = db.Ping()
	if err != nil {
		t.Fatalf("Failed to connect to Postgresql server: %s", err)
	}
	sdb, err := NewDB(db)
	if err != nil {
		t.Fatalf("Failed to create Squalus DB: %s", err)
	}

	_, err = sdb.Exec(context.Background(), "drop table if exists [persons]", nil)
	assert.NoError(t, err, "Table destruction should succeed")
	_, err = sdb.Exec(
		context.Background(),
		"create table [persons]([id] int PRIMARY KEY, "+
			"[name] varchar(128), [height] float, [birth] timestamp with time zone)",
		nil,
	)
	assert.NoError(t, err, "Table creation should succeed")

	_, err = sdb.Exec(context.Background(), "set time zone 'UTC'", nil)
	assert.NoError(t, err, "Setting time zone should succeed")

	_, err = sdb.Exec(context.Background(), "drop table if exists testbinary", nil)
	assert.NoError(t, err, "Table destruction should succeed")
	_, err = sdb.Exec(context.Background(), "create table testbinary([data] bytea)", nil)
	assert.NoError(t, err, "Table creation should succeed")

	return "postgresql", sdb, db
}

func TestPostgresqlContext(t *testing.T) {
	_, sdb, _ := ConnectToPostgresql(t)
	defer func() { assert.NoError(t, sdb.Close()) }()

	ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond)
	defer cancel()
	_, err := sdb.Exec(
		ctx,
		"SELECT pg_sleep(1)", // Argument to pg_sleep is in seconds
		nil,
	)
	assert.Error(t, err)
	assert.Equal(t, context.DeadlineExceeded, ctx.Err())
}

func TestPostgresqlUnsuportedIsolationLevel(t *testing.T) {
	_, sdb, _ := ConnectToPostgresql(t)
	defer func() { assert.NoError(t, sdb.Close()) }()

	_, err := sdb.Begin(context.Background(), &sql.TxOptions{Isolation: sql.LevelSnapshot})
	assert.Error(t, err)
}

func TestPostgresqlUpsert(t *testing.T) {
	_, sdb, _ := ConnectToPostgresql(t)
	defer func() { assert.NoError(t, sdb.Close()) }()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	_, err := sdb.Exec(
		ctx,
		"INSERT INTO [persons] ([id], [name]) VALUES ({id}, {name}) ON CONFLICT ([id]) DO UPDATE SET [name] = {name}",
		map[string]interface{}{
			"id":   100,
			"name": "NAME",
		},
	)
	assert.NoError(t, err)
}
