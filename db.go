package squalus

import (
	"context"
	"database/sql"
	"fmt"
	"reflect"
	"strings"
)

// DB represents a Squalus database connection.
//
// Begin starts a new transaction.
//
// Close frees the resources allocated to this connection.
type DB interface {
	SQLLink
	Begin(ctx context.Context, opts *sql.TxOptions) (Tx, error)
	Close() error
}

type dbQuerier struct {
	db  *sql.DB
	drv driver
}

func (dbq dbQuerier) driver() driver {
	return dbq.drv
}

func (dbq dbQuerier) ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	return dbq.db.ExecContext(ctx, query, args...)
}

func (dbq dbQuerier) QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error) {
	return dbq.db.QueryContext(ctx, query, args...)
}

func (dbq dbQuerier) QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row {
	return dbq.db.QueryRowContext(ctx, query, args...)
}

// connection represents a database connection.
type connection struct {
	db     *sql.DB
	driver driver
}

// sqldb returns the underlying SQL database.
func (cnx *connection) sqldb() *sql.DB {
	return cnx.db
}

// NewDB returns a new database connection.
func NewDB(db *sql.DB) (DB, error) {
	// detect underlying database type and select corresponding driver.
	var drv driver
	dbType := reflect.TypeOf(db.Driver()).String()
	switch {
	case strings.Contains(dbType, "MySQL"):
		drv = mysqlDriver{}
	case strings.Contains(dbType, "SQLite"):
		drv = mysqlDriver{} // SQLite is compatible with MySQL as far as Squalus is concerned.
	case strings.Contains(dbType, "pq"):
		drv = postgresqlDriver{}
	case strings.Contains(dbType, "mssql"):
		drv = mssqlDriver{}
	default:
		return nil, fmt.Errorf("unsupported database driver: %s", dbType)
	}
	res := connection{db: db, driver: drv}
	return &res, nil
}

// Close closes the underlying connection.
func (cnx *connection) Close() error {
	return cnx.db.Close()
}

// Query runs a query with parameters and returns the results.
func (cnx *connection) Query(
	ctx context.Context, query string, params map[string]interface{}, to interface{},
) error {
	return internalQuery(ctx, &dbQuerier{drv: cnx.driver, db: cnx.sqldb()}, query, params, to)
}

// Exec executes a query without returning any rows.
func (cnx *connection) Exec(ctx context.Context, query string, params map[string]interface{}) (sql.Result, error) {
	return internalExec(ctx, &dbQuerier{drv: cnx.driver, db: cnx.sqldb()}, query, params)
}

// Begin starts a transaction.
func (cnx *connection) Begin(ctx context.Context, opts *sql.TxOptions) (Tx, error) {
	tx, err := cnx.db.BeginTx(ctx, opts)
	if err != nil {
		return nil, err
	}
	return &transaction{drv: cnx.driver, tx: tx}, nil
}
