package squalus

type postgresqlDriver struct{}

func (d postgresqlDriver) adaptQuery(query string, params map[string]interface{}) (string, []interface{}, error) {
	return adaptQueryNumbered(replaceSquareBracketsWithDoubleQuotes(query), params, "$")
}
