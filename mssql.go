package squalus

type mssqlDriver struct{}

func (d mssqlDriver) adaptQuery(query string, params map[string]interface{}) (string, []interface{}, error) {
	return adaptQueryNumbered(query, params, "@p")
}
