package squalus

import (
	"context"
	"database/sql"
	"testing"
	"time"

	_ "github.com/denisenkom/go-mssqldb"
	"github.com/stretchr/testify/assert"
)

func ConnectToMssql(t *testing.T) (string, DB, *sql.DB) {
	db, err := sql.Open("sqlserver", "port=11433;user id=sa;password=abcDEF12;")
	errPing := db.Ping()
	if err != nil || errPing != nil {
		db, err = sql.Open("sqlserver", "server=mssql;port=1433;user id=sa;password=abcDEF12;")
		if err != nil {
			t.Fatalf("Failed to connect to MS SQL server: %s", err)
		}
	}
	err = db.Ping()
	if err != nil {
		t.Fatalf("Failed to connect to MS SQL server: %s", err)
	}
	sdb, err := NewDB(db)
	if err != nil {
		t.Fatalf("Failed to create Squalus DB: %s", err)
	}

	_, err = sdb.Exec(context.Background(), "drop table if exists [persons]", nil)
	assert.NoError(t, err, "Table destruction should succeed")
	_, err = sdb.Exec(
		context.Background(),
		"create table [persons]([id] int, [name] varchar(128), [height] float, [birth] datetime)",
		nil,
	)
	assert.NoError(t, err, "Table creation should succeed")

	_, err = sdb.Exec(context.Background(), "drop table if exists testbinary", nil)
	assert.NoError(t, err, "Table destruction should succeed")
	_, err = sdb.Exec(context.Background(), "create table testbinary([data] varbinary(max))", nil)
	assert.NoError(t, err, "Table creation should succeed")

	return "mssql", sdb, db
}

func TestMssqlContext(t *testing.T) {
	_, sdb, _ := ConnectToMssql(t)
	defer func() { assert.NoError(t, sdb.Close()) }()

	ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond)
	defer cancel()
	_, err := sdb.Exec(
		ctx,
		"WAITFOR DELAY '00:00:01'",
		nil,
	)
	assert.Error(t, err)
	assert.Equal(t, context.DeadlineExceeded, ctx.Err())
}

func TestMssqlUnsuportedIsolationLevel(t *testing.T) {
	_, sdb, _ := ConnectToMssql(t)
	defer func() { assert.NoError(t, sdb.Close()) }()

	_, err := sdb.Begin(context.Background(), &sql.TxOptions{Isolation: sql.LevelLinearizable})
	assert.Error(t, err)
}

func TestMssqlUpsert(t *testing.T) {
	_, sdb, _ := ConnectToMssql(t)
	defer func() { assert.NoError(t, sdb.Close()) }()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	_, err := sdb.Exec(
		ctx,
		`MERGE [persons] AS [target]
         USING (SELECT {id}, {name}) AS [source] ([id], [name])
         ON [target].[id] = [source].[id]
         WHEN MATCHED THEN UPDATE
           SET [name] = [source].[name]
         WHEN NOT MATCHED THEN
           INSERT ([id], [name])
           VALUES ({id}, {name});`,
		map[string]interface{}{
			"id":   100,
			"name": "NAME",
		},
	)
	assert.NoError(t, err)
}
