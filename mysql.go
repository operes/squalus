package squalus

import (
	"fmt"
	"reflect"
	"strings"
)

type mysqlDriver struct{}

func (d mysqlDriver) adaptQuery(query string, params map[string]interface{}) (string, []interface{}, error) {
	myQuery := replaceSquareBracketsWithBackticks(query)

	infos := getParamsInfos(params)
	// build a slice with the values of all {args}
	var myArgs []interface{}
	re := paramsRegexp()
	matches := re.FindAllString(myQuery, -1)

	// replace {args} with the appropriate number of ?
	myQuery = re.ReplaceAllStringFunc(
		myQuery,
		func(name string) string {
			i, ok := infos[name]
			if !ok {
				return ""
			}
			if i.length == 0 {
				return ""
			}
			return "?" + strings.Repeat(",?", i.length-1)
		},
	)

	for _, param := range matches {
		i, ok := infos[param]
		if !ok {
			return "", nil, fmt.Errorf("parameter %s was not given a value", param)
		}
		if i.length == 1 {
			myArgs = append(myArgs, i.value)
		} else {
			for j := 0; j < i.length; j++ {
				myArgs = append(myArgs, reflect.ValueOf(i.value).Index(j).Interface())
			}
		}
	}

	return myQuery, myArgs, nil
}
