package squalus

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"reflect"
)

type driver interface {
	adaptQuery(query string, params map[string]interface{}) (string, []interface{}, error)
}

type querier interface {
	driver() driver
	ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error)
	QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error)
	QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row
}

// SQLLink contains the methods that send a query to a database.
//
// Exec runs a query that does not return rows.
//
// Query runs a query that returns rows and stores the result in to.
type SQLLink interface {
	Exec(ctx context.Context, query string, params map[string]interface{}) (sql.Result, error)
	Query(ctx context.Context, query string, params map[string]interface{}, to interface{}) error
}

// selectAllRows reads all rows for the query and arguments, calling the function for each row.
func selectAllRows(
	ctx context.Context, q querier, query string, args []interface{}, cb func(rows *sql.Rows) error,
) (outErr error) {
	rows, err := q.QueryContext(ctx, query, args...)
	if err != nil {
		return err
	}
	defer func() {
		if err := rows.Close(); err != nil && outErr == nil {
			outErr = err
		}
	}()
	for rows.Next() {
		if err := cb(rows); err != nil {
			return err
		}
	}
	return rows.Err()
}

func scanDirect(typ reflect.Type) bool {
	_, b := reflect.PtrTo(typ).MethodByName("Scan")
	if !b {
		b = typ.String() == "time.Time"
	}
	return b
}

// scanRowIntoNewValue scans the values in a row into a new instance of the given type and returns it.
func scanRowIntoNewValue(rows *sql.Rows, rType reflect.Type) (*reflect.Value, error) {
	switch rType.Kind() {
	case reflect.Slice:
		if rType.Elem().Kind() != reflect.Uint8 {
			return nil, fmt.Errorf("unsupported type for scanning SQL query results: %v", rType)
		}
		fallthrough // byte slices are treated like basic types
	case reflect.Bool, reflect.String,
		reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
		reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64,
		reflect.Float32, reflect.Float64:
		val := reflect.New(rType)
		if err := rows.Scan(val.Interface()); err != nil {
			return nil, err
		}
		e := val.Elem()
		return &e, nil
	case reflect.Struct:
		scanDirect := scanDirect(rType)
		val := reflect.New(rType)
		columns, err := rows.Columns()
		if err != nil {
			return nil, err
		}
		dests := []interface{}{val.Interface()}
		if !scanDirect {
			var err error
			dests, err = destsForStruct(reflect.Indirect(val), columns)
			if err != nil {
				return nil, err
			}
		}
		if err := rows.Scan(dests...); err != nil {
			return nil, err
		}
		e := val.Elem()
		return &e, nil
	}
	return nil, fmt.Errorf("unsupported type for scanning SQL query results: %v", rType)
}

// internalQuery runs a query with parameters and returns the results.
func internalQuery(
	ctx context.Context, q querier, query string, params map[string]interface{}, to interface{},
) (outErr error) {
	adQuery, adArgs, err := q.driver().adaptQuery(query, params)
	if err != nil {
		return err
	}
	var values []reflect.Value
	var scanValues []interface{}
	toVal := reflect.ValueOf(to)
	toType := toVal.Type()
	switch toType.Kind() {
	case reflect.Ptr:
		switch toType.Elem().Kind() {
		case reflect.Struct:
			if scanDirect(toType.Elem()) {
				return q.QueryRowContext(ctx, adQuery, adArgs...).Scan(to)
			}
			rows, err := q.QueryContext(ctx, adQuery, adArgs...)
			if err != nil {
				return err
			}
			defer func() {
				if errClose := rows.Close(); errClose != nil && outErr == nil {
					outErr = errClose
				}
			}()
			if !rows.Next() {
				return sql.ErrNoRows
			}
			columns, err := rows.Columns()
			if err != nil {
				return err
			}
			dests, err := destsForStruct(toVal.Elem(), columns)
			if err != nil {
				return err
			}
			if err := rows.Scan(dests...); err != nil {
				return err
			}
			return rows.Err()
		case reflect.Slice:
			if toType.Elem().Elem().Kind() != reflect.Uint8 {
				slice := reflect.New(toType.Elem()).Elem()
				if err := selectAllRows(ctx, q, adQuery, adArgs, func(rows *sql.Rows) error {
					value, err := scanRowIntoNewValue(rows, slice.Type().Elem())
					if err != nil {
						return err
					}
					slice = reflect.Append(slice, *value)
					return nil
				}); err != nil {
					return err
				}
				toVal.Elem().Set(slice)
				return nil
			}
			fallthrough // []byte is treated like a basic type
		case reflect.Bool, reflect.String,
			reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
			reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64,
			reflect.Float32, reflect.Float64:
			return q.QueryRowContext(ctx, adQuery, adArgs...).Scan(to)
		}
	case reflect.Chan:
		defer toVal.Close()
		return selectAllRows(ctx, q, adQuery, adArgs, func(rows *sql.Rows) error {
			value, err := scanRowIntoNewValue(rows, toType.Elem())
			if err != nil {
				return err
			}
			toVal.Send(*value)
			return nil
		})
	case reflect.Func:
		for i := 0; i < toType.NumIn(); i++ {
			v := reflect.New(toType.In(i))
			values = append(values, v)
			scanValues = append(scanValues, v.Interface())
		}
		return selectAllRows(ctx, q, adQuery, adArgs, func(rows *sql.Rows) error {
			if err := rows.Scan(scanValues...); err != nil {
				return err
			}
			var cbValues []reflect.Value
			for _, v := range values {
				cbValues = append(cbValues, reflect.Indirect(v))
			}
			results := toVal.Call(cbValues)
			switch len(results) {
			case 0:
				return nil
			case 1: // the callback returned a result, check if it is an error.
				if results[0].Type().Implements(reflect.TypeOf((*error)(nil)).Elem()) {
					if results[0].IsNil() {
						return nil // this particular case is necessary because .Interface() crashes for a nil value
					}
					return results[0].Interface().(error)
				}
				return fmt.Errorf("type of callback return value is not error but %v", results[0].Type())
			default:
				return errors.New("callback function returned more than one value")
			}
		})
	}
	return fmt.Errorf("squalus.Query: type of to (%T) is not supported", reflect.TypeOf(to))
}

// internalExec executes a query without returning any rows.
func internalExec(ctx context.Context, q querier, query string, params map[string]interface{}) (sql.Result, error) {
	adQuery, adParams, err := q.driver().adaptQuery(query, params)
	if err != nil {
		return nil, err
	}
	return q.ExecContext(ctx, adQuery, adParams...)
}
